##=======================================================================================
# Name:        Victor Hugo Sillerico Justo
# No. USP:     11904461
# Course Code: SCC5830 (Image Processing)
# Year:        2020
# Semester:    I
# FINAL PROJECT
##=======================================================================================

# libraries required
import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
import imageio
import math
import matplotlib as mpl

# Getting the Left and Right images from a Stereo Camera
filename_L = str(input()).rstrip()
filename_R = str(input()).rstrip()
imgL = imageio.imread(filename_L) 
imgR = imageio.imread(filename_R) 


# =================================================================
# First Approach: Denoising Process of the Disparity Map 

# Getting the Disparity Map
# forces similar disparity on neighbouring blocks
stereo = cv.StereoSGBM_create(numDisparities=256, blockSize=15) 
# computes disparity map for the specified stereo pair.
disparity = stereo.compute(imgL,imgR)

# specified the kernel to perform filtering
kernel = np.ones((5,5),np.uint8) 

# Scaling and converting the Disparity Map to the required format

# get a Disparity Map in UINT8 format
maxDisp = np.max(disparity)
modDisparity = np.where(disparity<0,0,disparity)
modDisparity = np.array((modDisparity/maxDisp)*255,dtype=np.uint8)

# Erotion and Dilation of the Modified Disparity Map

# perform opening operation, so apply erotion first and to that result apply dilation
# erodes an image by using a specific structuring element
erosionMDP = cv.erode(modDisparity,kernel,iterations = 1)
# dilates an image by using a specific structuring element
dilationMDP = cv.dilate(erosionMDP,kernel,iterations = 4)

# Denoising the Modified Disparity Map

# apply median filter to deal with noise in the image
# blurs an image using the median filter
denoisedDisp = cv.medianBlur(dilationMDP, 35)

# Generating the required Mask for the Inpainting Process

# specified the aproppriate size of the mask 
r,c = modDisparity.shape
# create a mask to use with the Disparity Map without noise
mask = np.zeros((r,c), dtype=np.uint8)
mask = np.where(denoisedDisp<=10, 255, 0)
mask = np.array(mask,dtype=np.uint8)
# create a mask to use with the original Disparity Map with imperfections
maskInit = np.zeros((r,c), dtype=np.uint8)
maskInit = np.where(modDisparity==0, 255, 0)
maskInit = np.array(maskInit,dtype=np.uint8)

# Inpainting Process with the Original Disparity Map

# restores the selected region in an image using the region neighborhood
dst = cv.inpaint(modDisparity,maskInit,9,cv.INPAINT_NS)

# Inpainting Process with the Enhaced (without noise) Disparity Map

# restores the selected region in an image using the region neighborhood
dstDDP = cv.inpaint(denoisedDisp,mask,9,cv.INPAINT_NS)

# =================================================================
# Second Approach: Enhacement  Process of the Mask

# Getting edges

# modified the mask required by the inpainting algorithm to get 
# the object's borders well-defined after the inpainting

# finds edges in the input image and marks them in the output 
# map edges using the Canny algorithm
edges = cv.Canny(imgL,50,100) 

# apply inverse to mark the borders with black lines
edgesInv = np.where(edges==0,255,0)

# Dilation process to improve border representation
# dilates the image of edges by using an specific structuring element
edgesDil = cv.dilate(edges,kernel,iterations = 4)
# apply inverse to mark the borders with black lines
edgesInvDil = np.where(edgesDil==0,255,0)

# Combining the Inverted Dilated Edges image with the Mask

# create a new mask which allows a better representation of object's borders
# during the inpainting process
modMask = np.where(mask==255, edgesInvDil, mask)
modMask = np.array(modMask,dtype=np.uint8)

# Inpainting Process with the Enhaced (without noise) Disparity Map and the Combined Mask

# restores the selected region in an image using the region neighborhood
dstPartial = cv.inpaint(denoisedDisp,modMask,9,cv.INPAINT_NS)


# Generating a simple Mask from the Partial Disparity Map

# create a new simple mask to perform inpainting with the partial map
finalMask = np.zeros((r,c), dtype=np.uint8)
finalMask = np.where(dstPartial<=30, 255, 0)
finalMask = np.array(finalMask,dtype=np.uint8)

# Final Inpainting Process with the Partial Disparity Map and the Final Mask

# restores the selected region in an image using the region neighborhood
dstFinal = cv.inpaint(dstPartial,finalMask,3,cv.INPAINT_NS)

dstFinal = dstFinal.astype(np.uint8)
imageio.imwrite("newDisparityMap.png",dstFinal)






