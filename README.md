# DIP Final Project (SCC5830)
## Inpainting approach to enhace disparity maps used in mobile robot navigation
### Project Developer
- Victor Hugo Sillerico Justo 
- USP number: 11904461
- Graduate Student (MSc.)

## Abstract
In this project, an inpainting technique is used to improve the quality of 
disparity maps in order to get dense and precise information about the depth in the scene.
Some objects have textureless, reflective and semi-trasparent nature, therefore
their representation on disparity maps present noise and holes, which negatively
influences the information of depht that mobile robots use to navigate and
performe tasks in dynamic environments. Depth estimation is critical for robots
such as self-driving cars that actuate in urban scenarios, stereo matching 
technology aims to replace LiDAR sensors that are very expensive but are widely
used for depth estimation, however it is necesary to develop post-filtering
procedures involving restoration and segmentation to clean, and to align the disparity map edges with those of the original
image to increase the reliability of the information provided by the scene.  

### Application:
Binocular stereo matching technology is an interesting and low-cost alternative 
to LiDAR devices to generate dense depth estimation using disparity maps.
Perceiving the surroundings accurately and quickly is an essential and challenging
 task in the perception stage of mobile robot autonomous navigation. 
For example, self-driving cars relay on the afformentioned technology to
 detect obstacles on the road, or to stop in the presence of pedestrians or
 other cars at intersections.

## 1. Main Objective
The main goal is to make a refinement of disparity maps using different image
 processing techniques in order to invalidate innacurate values, and to fill
 in the holes presented in the disparity map, in that way the depth information
 provided by the disparity map will be more reliable.

## 2. Input Data Description 
The input images for the project were obtained from [Middlebury Data Set](http://vision.middlebury.edu/stereo/data/) and from [KITTI Data Set](http://www.cvlibs.net/datasets/kitti/eval_scene_flow.php?benchmark=stereo) .

A single input set is composed by two images (left and right) which were obtained
 by displacing horizontally two cameras in order to get differing views on the same scene. Those images were used to
 get the Disparity Map which provides depth information 
of the objects in the scene. 

#### Examples of Input Images

#### Input Data 1: "Recycle" (Left Image)
![Left Image](/images/Recycle_L.png)
#### Input Data 1: "Recycle" (Right Image)
![Right Image](/images/Recycle_R.png)
#### Input Data 2: "Storage" (Left Image)
![Left Image](/images/Storage_L.png)
#### Input Data 2: "Storage" (Right Image)
![Right Image](/images/Storage_R.png)

## 3. Description of the Methodology
### Step 1: Denoising process of the Disparity Map

It was necessary to scaled (0-255) and to convert the Disparity Map to the format (UINT8) required by the Inpainting Algorithm. 

The Disparity Map was obtained considering the Left and Right images of the same scene, for that purpose openCV functions were used:

- cv.StereoSGBM_create(): Semi-Global Block Matching forces similar disparity on neighbouring blocks. This creates a more complete disparity map but is more computationally expensive. 
- cv.stereo.compute(): Computes disparity map for the specified stereo pair.

The disparity map usually presents some black holes and noise, specially when the objects in the scene are less-texture, so the first procedure is concern with denoising. 

Morphological operations such as erosion and dilation were applied to eliminate or at least minimize those undesirable components of the image, for that purpose openCV functions were used:

- cv.erode(): Erodes an image by using a specific structuring element.
- cv.dilate(): Dilates an image by using a specific structuring element.

In order to remove the remanent undesirable values, a denoising process was made with a median filter, a openCV function was used:

- cv.medianBlur(): Blurs an image using the median filter. 

Also, the mask was computed based on the information of the Disparity Map.

Then, the inpainting algorithm based on the Navier-Stokes method was used.

- cv.inpaint(): Restores the selected region in an image using the region neighborhood.
Inpainting method that could be one of the following:
INPAINT_NS Navier-Stokes based method.
INPAINT_TELEA Method by Alexandru Telea

After applying the Inpainting Algorithm, it was possible to appreciate that most of the big dark holes disappeared or they considerably reduced in size, which means that the depth information obtained from the new Disparity Map is more reliable.

### Step 2: Enhancement of the Mask
Second, the Disparity Map, which resulted after the denoising task in First Step, was combined with a new mask, the strategy adopted was to improve the mask required to apply the inpainting algorithm a second time.

An edge-detection process was necessary to get the borders of the different objects presented in the scene, for that purpose openCV functions were used:

- cv.Canny(): The function finds edges in the input image and marks them in the output map edges using the Canny algorithm. The smallest value between threshold1 and threshold2 is used for edge linking. The largest value is used to find initial segments of strong edges.

An inversion process was applied on the mask to mark the borders with black lines instead of white lines. After that, a dilation operation was executed to obtain thicker lines to identify the edges in the scene. 

A new mask was created combining the previous single mask from step 1 and the
image that describes the borders. 

Then, the Inpainting Algorithm was applied using the new mask and the previous disparity map from step 1 in which the noise was reduced, to create a so called Partial Disparity Map. 

A final simple mask was created from that partial map of disparities, and the Inpainting Algorithm was called again, the result is a Disparity Map without dark wholes and less noise.

  
To sum up, the following procedures were used in the project:
- Image Enhancement.
- Image Filtering to reduce the noise.
- Edge-Detection.
- Morphology (Dilation).
- Image Inpainting.

## 4. Initial Code with First Results:

The following notebooks show the results of the methodology applied to reach the objective:

### Indoor Scenes:
- [Notebook Indoor Scene 1](DIP-FP-Indoor-Scene1.ipynb).
- [Notebook Indoor Scene 2](DIP-FP-Indoor-Scene2.ipynb).
- [Notebook Indoor Scene 3](DIP-FP-Indoor-Scene3.ipynb).

### Outdoor Scenes:
- [Notebook Outdoor Scene 1](DIP-FP-Outdoor-Scene1.ipynb).
- [Notebook Outdoor Scene 2](DIP-FP-Outdoor-Scene2.ipynb).
- [Notebook Outdoor Scene 3](DIP-FP-Outdoor-Scene3.ipynb).

## 5.Discussion of Results (Cases of Success and Failures)

### Indoor Scenes

Images of indoor scenarios were used to study the performance of the proposed  methodology to enhance the Disparity Maps.

The first approach generate a Disparity Map with few noise and reduced black holes when compared with the original disparity map. However, it is clear that when there are texture-less objects in the scene, it is not possible to make a complete inpainting process in all parts of those objects.

Morphological operation such as Dilation and Erosion were useful to  eliminate small black holes in the original disparity map, They also were useful to fill regions inside objects with the appropriate neighboring color. The blur process using a median filter was effective to eliminate the remained noise.

The mask was obtained from the Disparity Map without noise described above, so when the inpaining process was applied, the result was a better Map of Disparities that presented less inaccuracies compared to the original. However, the geometries of the objects were diminish an presented some deformations.   

The Second approach used the Disparity Map without noise obtained in the First approach, and additionally used an “enhanced mask” which combined the simple mask and an image with edges of the original left image. This new mask attempted to preserve the geometries and avoid deformations of the objects during the inpainting process. It was possible to observe an enhancement in the so called Partial Disparity Map, some objects corrected or kept their borders and they tried to preserve their original shape. After that, the inpainting algorithm was used again to completely eliminate black holes in the Partial Disparity Map.

It was observed that in indoor scenes the proposed methodology successfully improve the quality of disparity maps. However, when the image has texture-less objects, those present some imperfections in the final result.   

### Outdoor scenes

Outdoor scenes processing resulted to be more challenging, the proposed methodology help to improve the disparity maps in the same way described for indoor scenes. However, there is still a lot of work to do with scenes of unstructured environments. Some problems were related again with the texture-less objects, also long segments such as roads, sky, buildings, and others, are not appropriately represented in the final disparity map.   

## 6. Video Presentation
- [Project Description](https://youtu.be/gM1WUjTYbr8)
